#include "Mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    // Application settings
    a.setApplicationName("Audio Game");
    a.setOrganizationName("Max Planck Institute for Brain Research");
    a.setOrganizationDomain("brain.mpg.de");

    MainWindow w;
    w.show();

    return a.exec();
}
