#include "trialseq.h"
namespace TrialSeq {

int randomGenerator (int i) { return rand()%i; }

double randomMinMax (double min, double max) {
    std::mt19937 rng;
    std::uniform_real_distribution<double> dist(min, max);  //(min, max)
    rng.seed(std::random_device{}()); //non-deterministic seed
    return dist(rng);
}

/*Generate the sequence (Negative always first*/
QVector<int> generateTrialSequence(int sorting, int repeatPositive, int repeatNegative)
{
    QVector<int> trialSequence;
    int currentRepeatPositive;
    int currentRepeatNegative;

    switch(sorting){
        case TrialSeq::ORDERED:
            currentRepeatPositive = 1;
            currentRepeatNegative = 1;
            while(currentRepeatNegative <= repeatNegative || currentRepeatPositive <= repeatPositive) {
                if(currentRepeatNegative <= repeatNegative) {
                    trialSequence.append(0);
                    currentRepeatNegative++;
                }
                if(currentRepeatPositive <= repeatPositive) {
                    trialSequence.append(1);
                    currentRepeatPositive++;
                }
            }
            break;
        case TrialSeq::SORTED:
            for (currentRepeatNegative = 1; currentRepeatNegative <= repeatNegative; ++currentRepeatNegative) {
                trialSequence.append(0);
            }
            for (currentRepeatPositive = 1; currentRepeatPositive <= repeatPositive; ++currentRepeatPositive) {
                trialSequence.append(1);
            }

            break;
        case TrialSeq::SHUFFLED:
            for (currentRepeatNegative = 1; currentRepeatNegative <= repeatNegative; ++currentRepeatNegative) {
                trialSequence.append(0);
            }
            for (currentRepeatPositive = 1; currentRepeatPositive <= repeatPositive; ++currentRepeatPositive) {
                trialSequence.append(1);
            }

            // shuffle trial sequence
            std::random_shuffle(trialSequence.begin(), trialSequence.end(), TrialSeq::randomGenerator);
            break;
        default:
            for (currentRepeatNegative = 1; currentRepeatNegative <= repeatNegative; ++currentRepeatNegative) {
                trialSequence.append(0);
                for (currentRepeatPositive = 1; currentRepeatPositive <= repeatPositive; ++currentRepeatPositive) {
                    trialSequence.append(1);
                }
            }
    }
    qDebug() << "TrialSequence: " << trialSequence;
    return trialSequence;

}

QVector<double> generateItiSequence(double userReaction, QString itiList, double preImaq, int repeatPositive, int repeatNegative)
{
    QVector<double> itiSequence;
    int itiSequenceLength = repeatPositive + repeatNegative;
    if(!itiList.isEmpty()) {
        if(itiList.contains(',')) {
            QStringList iti_range = itiList.split(',');
            double itiValueMin = iti_range.first().toDouble() - userReaction - preImaq;
            double itiValueMax = iti_range.last().toDouble() - userReaction - preImaq;
            for (int itiSequenceElement = 0; itiSequenceElement < itiSequenceLength; ++itiSequenceElement) {
                itiSequence.append(TrialSeq::randomMinMax(itiValueMin, itiValueMax));
            }
        } else {
            double itiValue = itiList.toDouble() - userReaction - preImaq;
            for (int itiSequenceElement = 0; itiSequenceElement < itiSequenceLength; ++itiSequenceElement) {
                itiSequence.append(itiValue);
            }
        }
    }

    return itiSequence;
}


QVector<double> loadExternalItiSequence(QString fileName, int repeatPositive, int repeatNegative)
{
    QVector<double> itiSequence;

    // get iti duration value from external file
    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QTextStream in(&file);
        while (!in.atEnd()) {
            QString line = in.readLine();
            itiSequence.append(line.toDouble()); /*Get proper value*/
        }
    }else{
        qDebug()<<"Unable to open external ITI file";
    }
    return itiSequence;
}

}
