#include "TDTInterface.h"

/* initialize TDT Sound System */
int TDTInterface::initializeInterface()
{

    /* connect to RP2 system */
    if (!rp.ConnectRP2("USB", 1))
    {
        fprintf(stderr, "TDTError:TDTInterface/TDTInitializeInterface:\n\tfailed to connect to RP2!\n");
        return(EXIT_FAILURE);
    }

    /* check status */
    tdt_status = rp.GetStatus();

    /* check if device is connected */
    if ((tdt_status & TDT_STATUS_CONNECTED) == TDT_STATUS_CONNECTED)
    {
        printf("TDT RP2 connected\n");

        /* check if device is running */
        if ((tdt_status & TDT_STATUS_CIRCUIT_RUNNING) == TDT_STATUS_CIRCUIT_RUNNING)
            rp.Halt();

        /* check if device memory is full */
        if ((tdt_status & TDT_STATUS_CIRCUIT_LOADED) == TDT_STATUS_CIRCUIT_LOADED)
            rp.ClearCOF();
    }

    return 0;
}

/* destroy TDT Engine */
void TDTInterface::destroyInterface()
{
    /* check status */
    tdt_status = rp.GetStatus();

    /* check if device is connected */
    if ((tdt_status & TDT_STATUS_CONNECTED) == TDT_STATUS_CONNECTED)
    {
        /* check if device is running */
        if ((tdt_status & TDT_STATUS_CIRCUIT_RUNNING) == TDT_STATUS_CIRCUIT_RUNNING)
            rp.Halt();

        /* check if device memory is full */
        if ((tdt_status & TDT_STATUS_CIRCUIT_LOADED) == TDT_STATUS_CIRCUIT_LOADED)
            rp.ClearCOF();
    }
    rp.clear();
    return;
}

void TDTInterface::loadRCXCircuit(QString fileName)
{
    /* check status */
    tdt_status = rp.GetStatus();

    /* check if device is connected */
    if ((tdt_status & TDT_STATUS_CONNECTED) == TDT_STATUS_CONNECTED)
    {
        /* check if device is running */
        if ((tdt_status & TDT_STATUS_CIRCUIT_RUNNING) == TDT_STATUS_CIRCUIT_RUNNING)
            rp.Halt();

        /* check if device memory is full */
        if ((tdt_status & TDT_STATUS_CIRCUIT_LOADED) == TDT_STATUS_CIRCUIT_LOADED)
            rp.ClearCOF();

        /* load circuit */
        rp.LoadCOF(fileName);
		
        /* update status */
        tdt_status = rp.GetStatus();
        if ((tdt_status & TDT_STATUS_CIRCUIT_LOADED) == TDT_STATUS_NULL)
        {
            fprintf(stderr, "TDTError:TDTInterface/TDTLoadRCXCircuit:\n\tfailed to load %s\n%s\n", fileName);
            exit(EXIT_FAILURE);
        }

        /* run circuit */
        rp.Run();
    }
    return;
}
