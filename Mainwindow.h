#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QFile>
#include <QDebug>
#include <QSettings>
#include <QStandardPaths>
#include <QMessageBox>
#include "TimerDialog.h"
#include "StateMachineController.h"
#include "SettingStructures.h"
#include "trialseq.h"
#include "AboutDialog.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_selectOutputFolder_clicked();
    void on_checkBox_autoFilename_clicked(bool checked);
    void on_pushButton_selectFilePositive_clicked();
    void on_pushButton_selectFileNegative_clicked();

    void on_pushButton_Run_clicked();
    void on_pushButton_Pause_toggled(bool checked);
    void on_pushButton_Stop_clicked();
    void on_checkBox_itiExternal_clicked(bool checked);
    void on_lineEdit_iti_editingFinished();
    void on_lineEdit_preImaq_editingFinished();

    void onStateMachineTerminated();
    void onStatusMessage(QString statusMessage);

    void on_pushButton_about_clicked();

private:
    Ui::MainWindow *ui;

    StateMachineController smc;

    void updateIti();
    void generateAutoFileName();
    const double userReaction = 30; //sec

    const QString DEFAULT_DIR_KEY = "/home";
    QSettings appSettings;
    TimerDialog *timerDialog;

    QString selectedDirectory;
    QString selectedPositiveFileName;
    QString selectedNegativeFileName;
};

#endif // MAINWINDOW_H
