#include "TimerDialog.h"
#include "ui_timerdialog.h"

TimerDialog::TimerDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TimerDialog)
{
    ui->setupUi(this);

    timerDialogTimer = new QTimer(this);
    connect(timerDialogTimer, SIGNAL(timeout()), this, SLOT(onCountDown()));

}

TimerDialog::~TimerDialog()
{
    delete ui;
}

void TimerDialog::setUpTimer(int timeValue)
{
    this->timeValue = timeValue;
    ui->timeRemaining->setText(QString::number(timeValue));
}

void TimerDialog::updateTime()
{
    this->timeValue -= 1;
    ui->timeRemaining->setText(QString::number(this->timeValue));
}

void TimerDialog::startTimer()
{
    timerDialogTimer->start(1000);
}

void TimerDialog::stopTimer()
{
    timerDialogTimer->stop();
}

void TimerDialog::removeTimer()
{
    stopTimer();
    this->hide();
}

void TimerDialog::onCountDown()
{
    updateTime();
    // check if finished
    if(this->timeValue <= 0){
        removeTimer();
    }
}

