#ifndef TRIALSEQ_H
#define TRIALSEQ_H

#include <random>
#include <ctime>
#include <algorithm>
#include <QVector>
#include <QString>
#include <QFile>
#include <QTextStream>
#include <QDebug>

namespace TrialSeq {
    enum SORTING {ORDERED, SORTED, SHUFFLED};

    // values for trial sequence sorting
    int randomGenerator (int i);
    double randomMinMax (double min, double max);
    QVector<int> generateTrialSequence(int sorting, int repeatPositive, int repeatNegative);
    QVector<double> generateItiSequence(double userReaction, QString itiList, double preImaq, int repeatPositive, int repeatNegative);
    QVector<double> loadExternalItiSequence(QString fileName, int repeatPositive, int repeatNegative);

}

#endif // TRIALSEQ_H
