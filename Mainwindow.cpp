#include "Mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)

{
    ui->setupUi(this);

    /*Load defaults from settings*/
    ui->lineEdit_repeatPositive->setText(appSettings.value("defaults/repeatPositive", 10).toString());
    ui->lineEdit_repeatNegative->setText(appSettings.value("defaults/repeatNegative", 10).toString());
    ui->lineEdit_soundDurationPositive->setText(appSettings.value("defaults/soundDurationPositive", .5).toString());
    ui->lineEdit_soundDurationNegative->setText(appSettings.value("defaults/soundDurationNegative", .5).toString());

    ui->lineEdit_preDelay->setText(appSettings.value("defaults/preDelay", 0).toString());
    ui->lineEdit_preImaq->setText(appSettings.value("defaults/preImaq", 0).toString());
    ui->lineEdit_licksToReward->setText(appSettings.value("defaults/licksToReward", 10).toString());
    ui->lineEdit_iti->setText(appSettings.value("defaults/iti", 30).toString());

    ui->lineEdit_outputFolder->setText(appSettings.value("defaults/outputFolder", QStandardPaths::writableLocation(QStandardPaths::HomeLocation)).toString());

    if(appSettings.value("defaults/autoFilename", true).toBool()){
        ui->checkBox_autoFilename->click();
    }

    /*Set Validators*/
    QValidator *doubleValidator = new QDoubleValidator(0,1000,2, this);
    QValidator *intValidator = new QIntValidator(0,1000, this);

    ui->lineEdit_repeatPositive->setValidator(intValidator);
    ui->lineEdit_repeatNegative->setValidator(intValidator);
    ui->lineEdit_soundDurationPositive->setValidator(doubleValidator);
    ui->lineEdit_soundDurationNegative->setValidator(doubleValidator);
    ui->lineEdit_preDelay->setValidator(doubleValidator);
    ui->lineEdit_preImaq->setValidator(doubleValidator);
    ui->lineEdit_licksToReward->setValidator(intValidator);
    ui->lineEdit_iti->setValidator(doubleValidator);

//    ui->lineEdit_fileNegative->setText("C:/Users/superuser.D-01477/Desktop/attentionGame/20160812_AttentionGame/20160812_AttentionGame/Audio/negative_state.rcx");
//    ui->lineEdit_filePositive->setText("C:/Users/superuser.D-01477/Desktop/attentionGame/20160812_AttentionGame/20160812_AttentionGame/Audio/positive_state.rcx");

    connect(&smc, SIGNAL(statusMessage(QString)), this, SLOT(onStatusMessage(QString)));
    connect(&smc, SIGNAL(terminated()), this, SLOT(onStateMachineTerminated()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_Run_clicked()
{

    int sorting;
    switch(ui->comboBox_trialSequence->currentIndex()){
        case 0:
            sorting = TrialSeq::ORDERED;
            break;
        case 1:
            sorting = TrialSeq::SORTED;
            break;
        case 2:
            sorting = TrialSeq::SHUFFLED;
            break;
        default:
            sorting = TrialSeq::ORDERED;
    }

    /*Populate settings with GUI values*/
    audioGameSettings settings;
    settings.preImaq = ui->lineEdit_preImaq->text().toDouble();
    settings.preDelay = ui->lineEdit_preDelay->text().toDouble();
    settings.soundDurationPositive = ui->lineEdit_soundDurationPositive->text().toDouble();
    settings.soundDurationNegative = ui->lineEdit_soundDurationNegative->text().toDouble();
    settings.userReaction = userReaction;
    settings.trialSequence = TrialSeq::generateTrialSequence(sorting, ui->lineEdit_repeatPositive->text().toInt(), ui->lineEdit_repeatNegative->text().toInt());
    settings.fileNameNegative = ui->lineEdit_fileNegative->text();
    settings.fileNamePositive = ui->lineEdit_filePositive->text();
    settings.licksToReward = ui->lineEdit_licksToReward->text().toInt();

    if(ui->checkBox_autoFilename->isChecked()){
        generateAutoFileName();
    }
    settings.logFileName = ui->lineEdit_outputFolder->text() + "/" + ui->lineEdit_outputFilename->text() + ".csv";


    if(ui->checkBox_itiExternal->isChecked()){
        qDebug()<<"Using iti from file";
        settings.itiSequence = TrialSeq::loadExternalItiSequence("./iti_external.txt", ui->lineEdit_repeatPositive->text().toInt(), ui->lineEdit_repeatNegative->text().toInt());
    }else{
        settings.itiSequence = TrialSeq::generateItiSequence(userReaction, ui->lineEdit_iti->text(), ui->lineEdit_preImaq->text().toDouble(), ui->lineEdit_repeatPositive->text().toInt(), ui->lineEdit_repeatNegative->text().toInt());
    }

    if(!settings.itiSequence.isEmpty()){
        ui->pushButton_Run->setDisabled(true);
        ui->pushButton_Stop->setDisabled(false);
        ui->statusBar->showMessage("Running");
        smc.startStateMachine(settings);
    }else{
        QMessageBox::information(0, "error", "Unable to open iti_external.txt");
    }

}

void MainWindow::on_pushButton_Pause_toggled(bool checked)
{
    smc.pauseStateMachine();
    if (checked) {
        ui->statusBar->showMessage("Paused");
        ui->pushButton_Pause->setText("Resume");
    } else {
        ui->statusBar->showMessage("Resumed");
        ui->pushButton_Pause->setText("Pause");
    }
}

void MainWindow::on_pushButton_Stop_clicked()
{
    smc.stopStateMachine();
    qDebug() << "Stopped and terminated";
    ui->statusBar->showMessage("Stopped and terminated");
    ui->pushButton_Run->setDisabled(false);
    ui->pushButton_Stop->setDisabled(true);
    ui->pushButton_Pause->setChecked(false);
    ui->pushButton_Pause->setText("Pause");
}

void MainWindow::on_pushButton_selectOutputFolder_clicked()
{
    selectedDirectory = QFileDialog::getExistingDirectory(this, tr("Select Directory"),
                                                        appSettings.value(DEFAULT_DIR_KEY).toString(),
                                                        QFileDialog::ShowDirsOnly
                                                        | QFileDialog::DontResolveSymlinks);
    if (!selectedDirectory.isEmpty()) {
        QDir currentDir;
        // remember this directory next time the selector is opened
        appSettings.setValue(DEFAULT_DIR_KEY,
                            currentDir.absoluteFilePath(selectedDirectory));

        ui->lineEdit_outputFolder->setText(selectedDirectory);
    }
}

void MainWindow::on_checkBox_autoFilename_clicked(bool checked)
{
    // auto option checked
    if(checked) {
        ui->lineEdit_outputFilename->setText("YYYYMMDDhhmmss_AudioGame_events");
        ui->lineEdit_outputFilename->setDisabled(true);
    } else {
        // clear input field and enable it
        ui->lineEdit_outputFilename->clear();
        ui->lineEdit_outputFilename->setDisabled(false);
    }
}

void MainWindow::on_pushButton_selectFilePositive_clicked()
{
    selectedPositiveFileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open Audio File"), appSettings.value(DEFAULT_DIR_KEY).toString(), tr("Audio Files (*.rcx)"));

    if (!selectedPositiveFileName.isEmpty()) {
        QDir currentDir;
        // remember this directory next time the selector is opened
        appSettings.setValue(DEFAULT_DIR_KEY,
                            currentDir.absoluteFilePath(selectedPositiveFileName));

        ui->lineEdit_filePositive->setText(selectedPositiveFileName);
    }
}

void MainWindow::on_pushButton_selectFileNegative_clicked()
{
    selectedNegativeFileName = QFileDialog::getOpenFileName(this,
                                                                    tr("Open Audio File"), appSettings.value(DEFAULT_DIR_KEY).toString(), tr("Audio Files (*.rcx)"));

    if (!selectedNegativeFileName.isEmpty()) {
        QDir currentDir;
        // remember this directory next time the selector is opened
        appSettings.setValue(DEFAULT_DIR_KEY,
                            currentDir.absoluteFilePath(selectedNegativeFileName));

        ui->lineEdit_fileNegative->setText(selectedNegativeFileName);
    }
}


void MainWindow::on_checkBox_itiExternal_clicked(bool checked)
{
    if(checked) {
        ui->lineEdit_iti->setDisabled(true);
    } else {
        ui->lineEdit_iti->setDisabled(false);
    }
}

void MainWindow::on_lineEdit_iti_editingFinished()
{
    updateIti();
}

void MainWindow::on_lineEdit_preImaq_editingFinished()
{
    updateIti();
}

void MainWindow::onStateMachineTerminated()
{
    ui->pushButton_Run->setText("Run");
    ui->pushButton_Run->setDisabled(false);
    if(ui->checkBox_autoFilename->isChecked()){
        ui->lineEdit_outputFilename->setText("YYYYMMDDhhmmss_AudioGame_events");
    }
}

void MainWindow::onStatusMessage(QString statusMessage)
{
    ui->statusBar->showMessage(statusMessage);
}

void MainWindow::updateIti()
{
    double preImaq = ui->lineEdit_preImaq->text().toDouble();

    QString lineEdit_iti_str = ui->lineEdit_iti->text();
    if(!lineEdit_iti_str.isEmpty()) {
        if(lineEdit_iti_str.contains(',')) {
            QStringList iti_range = lineEdit_iti_str.split(',');
            if((iti_range.first().toDouble() < userReaction + preImaq) |
                    (iti_range.last().toDouble() < userReaction + preImaq)){
                ui->lineEdit_iti->setText(QString("%1,%2").arg(QString::number(iti_range.first().toDouble() + userReaction + preImaq), QString::number(iti_range.last().toDouble() + userReaction + preImaq)));
            }
        } else {
            if(ui->lineEdit_iti->text().toDouble() < userReaction + preImaq){
                ui->lineEdit_iti->setText(QString::number(ui->lineEdit_iti->text().toDouble() + userReaction + preImaq));
            }
        }
    }
}

void MainWindow::generateAutoFileName()
{
    time_t rawtime;
    struct tm * timeinfo;
    // ensure buffer is big enought
    char buffer[42];

    // current date/time based on current system
    time(&rawtime);
    // convert now to string form
    timeinfo = localtime(&rawtime);

    // write date/time info into formatted string
    strftime(buffer,42,"%Y%m%d%H%M%S_AudioGame_events",timeinfo);

    // write to input field and disable it
    ui->lineEdit_outputFilename->setText(QString(buffer));
}

void MainWindow::on_pushButton_about_clicked()
{
    AboutDialog* aboutDialog = new AboutDialog(this);
    aboutDialog->setAttribute(Qt::WA_DeleteOnClose);
    aboutDialog->show();
}
