#include "SocketClient.h"

SocketClient::SocketClient(QObject *parent) : QObject(parent)
{
    tcpSocket = new QTcpSocket(this);

    inBuffer.open(QIODevice::ReadWrite);
    inStream.setDevice(&inBuffer);
    inStream.setByteOrder(QDataStream::LittleEndian);
    inStream.setVersion(QDataStream::Qt_5_2);

    port = 20001;
    hostname = "127.0.0.1";
    this->connectToHost();
}

void SocketClient::onReadData()
{
    quint32 packetSize;
    quint32 packetType;

    inBuffer.seek(0);

    /*Read packet size*/
    readMax(tcpSocket, sizeof(packetSize));
    inBuffer.seek(0);
    inStream >> packetSize;

    /*Read packet type*/
    readMax(tcpSocket, sizeof(packetSize)+sizeof(packetType));
    inBuffer.seek(sizeof(packetSize));
    inStream >> packetType;

    /*Read content*/
    readMax(tcpSocket, packetSize);
    inBuffer.seek(sizeof(packetSize)+sizeof(packetType));

    int dataSize = packetSize-(sizeof(packetSize) + sizeof(packetType));

    switch(packetType){
        case SOCKET_FILENAME: {
                char* data = new char[dataSize];
                inStream >> data;
                QString fileName(data);
                delete data;
                emit(socketFileName(fileName));
                qDebug()<<"Socket: filename" << fileName;
            }
            break;
        case SOCKET_START: {
                quint64 timeStamp;
                inStream >> timeStamp;
                emit(socketStartTimeStamp(timeStamp));
                qDebug()<< "Socket: start timeStamp: " << timeStamp;
            }
            break;
        case SOCKET_STOP: {
                quint64 timeStamp;
                inStream >> timeStamp;
                emit(socketStopTimeStamp(timeStamp));
                qDebug()<< "Socket: stop timeStamp: " << timeStamp;
            }
            break;
        case SOCKET_TIMESTAMP: {
                quint64 timeStamp;
                inStream >> timeStamp;
                emit(socketTimeStamp(timeStamp));
                qDebug()<< "Socket: timeStamp: " << timeStamp;
            }
            break;
        case SOCKET_TRACKINGRESULT: {
                std::vector<double> data;
                data.resize(dataSize / sizeof(double));
                for(int i = 0; i< dataSize / sizeof(double); i++){
                    inStream >> data[i];
                }
                QVector<double> trackingResult = QVector<double>::fromStdVector(data);
                emit(socketTrackingResult(trackingResult));
            }
            break;
    }
    inBuffer.buffer().clear();
}

void SocketClient::connectToHost()
{
    tcpSocket->abort();
    tcpSocket->connectToHost(hostname,
                             port);
    if(!tcpSocket->waitForConnected(5000)){
        qDebug() << "Socket: Connection error: " << tcpSocket->errorString();
    }else{
        connect(tcpSocket, SIGNAL(connected()), this, SLOT(onConnected()));
        connect(tcpSocket, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
        connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(onReadData()), Qt::QueuedConnection);
        qDebug() << "Connected";
    }
}

void SocketClient::onConnected()
{
    qDebug()<< "Socket: Connected";
}

void SocketClient::onDisconnected()
{
    qDebug()<< "Socket: Disconnected";
}

void SocketClient::onSocketError()
{
    qDebug()<< "Socket: Error";
}

void SocketClient::readMax(QIODevice *io, int n)
 {
    while(inBuffer.size() < n){
      if(!io->bytesAvailable()){
          io->waitForReadyRead(30000);
      }
      inBuffer.write(io->read(n - inBuffer.size()));
    }
}
