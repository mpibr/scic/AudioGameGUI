#-------------------------------------------------
#
# Project created by QtCreator 2016-07-26T13:26:15
#
#-------------------------------------------------

# NOTE: COMPILES UNDER WINDOWS ONLY BECAUSE OF THE
# ACTIVE-X COM OBJECT TDT REQUIRES!! :-(

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
CONFIG += c++11
DEFINES += APP_VERSION=\\\"0.1\\\"
RESOURCES = application.qrc
RC_FILE = app.rc

QT += axcontainer

# QT automatically generates the classes from the com object
TYPELIBS = $$system(dumpcpp -getfile {D323A622-1D13-11D4-8858-444553540000})

isEmpty(TYPELIBS) {
    message("TDT library not found!")
} else {
    TYPELIBS = $$system(dumpcpp {D323A622-1D13-11D4-8858-444553540000})
    HEADERS += rpcoxlib.h
    SOURCES  += rpcoxlib.cpp
}

LIBS += -L"C:\\Program Files (x86)\\National Instruments\\Shared\\ExternalCompilerSupport\\C\\lib64\\msvc" \
    -lNIDAQmx

TARGET = AudioGameGUI
TEMPLATE = app

SOURCES += main.cpp\
    NIDAQmxInterface.cpp \
    StateMachineController.cpp \
    Mainwindow.cpp \
    TimerDialog.cpp \
    TDTInterface.cpp \
    trialseq.cpp \
    LogFileWriter.cpp \
    AboutDialog.cpp \
    SocketClient.cpp


HEADERS  += \
    NIDAQmxInterface.h \
    StateMachineController.h \
    TimerDialog.h \
    Mainwindow.h \
    SettingStructures.h \
    TDTInterface.h \
    trialseq.h \
    LogFileWriter.h \
    AboutDialog.h \
    SocketClient.h

FORMS    += mainwindow.ui \
    timerdialog.ui \
    AboutDialog.ui

DISTFILES +=
