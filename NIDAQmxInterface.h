#ifndef NIDAQmxInterface_h
#define NIDAQmxInterface_h

#include <QObject>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h> 
#include <time.h>
#include <QTime>
#include <QDebug>
#include "C:\Program Files (x86)\National Instruments\Shared\ExternalCompilerSupport\C\include\NIDAQmx.h"


int32 CVICALLBACK inputCallbackWrapper(TaskHandle taskHandle, int32 signalID, void *callbackData);
int32 CVICALLBACK pulldownTimerCallbackWrapper(TaskHandle taskHandle, int32 signalID, void *callbackData);

class NIDAQmxInterface: public QObject
{
    Q_OBJECT

    /* define DAQmxErrChk macro */
    #define DAQmxErrChk(functionCall)\
        if((functionCall) < 0)\
            {\
            char errBuffer[2048];\
            DAQmxGetExtendedErrorInfo(errBuffer, 2048);\
            fprintf(stderr, "DAQmxError: %s\n Calling: %s\n", errBuffer, #functionCall);\
            exit(EXIT_FAILURE);\
            }

    /* define DAQmxData type */
    typedef struct _DAQmxEngine
    {
        TaskHandle di_port;
        TaskHandle do_port;
        TaskHandle do_clk;
        TaskHandle di_error;
        uInt8	di_bit_now;
        uInt8	di_bit_prev;
        uInt8	do_bit_wrt;
        uInt8	do_bit_qry;
        uInt32	di_cntr_lick;
        uInt32	di_cntr_frame;
        uInt32  di_cntr_cameraTrigger;
        uInt32	di_cntr_imaq;
    } DAQmxEngine;


public:
    /* define port bit states */
    static const int DAQ_PORT_NULL = 0x00;
    static const int DAQ_PORT_TRUE = 0x01;

    static const int DAQ_PORT_DI_SHUTTER = 0x01;
    static const int DAQ_PORT_DI_BACKLIGHT = 0x02;
    static const int DAQ_PORT_DI_FRAME = 0x04;
    static const int DAQ_PORT_DI_CAMERATRIGGER = 0x08;

    static const int DAQ_PORT_DI_LICK = 0x40;
    static const int DAQ_PORT_DI_SCANNER = 0x80;

    static const int DAQ_PORT_DO_MONITOR = 0x01;
    static const int DAQ_PORT_DO_AIR = 0x02;
    static const int DAQ_PORT_DO_WATER = 0x04;
    static const int DAQ_PORT_DO_SOUND = 0x08;
    static const int DAQ_PORT_DO_IMGAQ = 0x10;
    static const int DAQ_PORT_DO_FREQGEN = 0x20;

    static const int DAQ_PORT_DE_MONITOR = 0x01;
    static const int DAQ_PORT_DE_AIR = 0x02;
    static const int DAQ_PORT_DE_WATER = 0x04;

    const double DO_PULSE_WIDTH = 0.08;// in seconds
    int licksToReward = 16;

    bool isPaused = false; //ignore input/output when true

    explicit NIDAQmxInterface(QObject *parent = 0);
    ~NIDAQmxInterface();
    /* function prototypes */
    void DAQmxInitializeInterface();
    void DAQmxDestroyInterface();
    void DAQmxSetDefaultEngine();
    void DAQmxReadErrorPort();
    void DAQmxStartCustomTask(TaskHandle);
    void DAQmxStopCustomTask(TaskHandle);
    void DAQmxTriggerDO(uInt8, bool);
    void DAQmxSetDO(uInt8, bool, bool);
    void DAQmxWriteDO(uInt8);
    DAQmxEngine* daq; /*MAKE PRIVATE*/

signals:
    void writeToLogFile(QString text, uint id, uint counter, QString stringTime);

private:
    /* events callback functions */
    int32 CVICALLBACK FcnCbckDetectDI(TaskHandle, int32);
    int32 CVICALLBACK FcnCbckTriggerStopDO(TaskHandle, int32);
    friend int32 CVICALLBACK inputCallbackWrapper(TaskHandle taskHandle, int32 signalID, void *callback_dat);
    friend int32 CVICALLBACK pulldownTimerCallbackWrapper(TaskHandle taskHandle, int32 signalID, void *callback_dat);
    bool errorCheck(int32 functionReturnValue);
    #endif /* define (NIDAQmxInterface_h) */
};
