#ifndef SETTINGSTRUCTURES_H
#define SETTINGSTRUCTURES_H
#include <QString>
#include <QVector>


struct audioGameSettings{

    QString logFileName;

    /*Sequences*/
    QVector<int> trialSequence;
    QVector<double> itiSequence;

    /*Timing*/
    double preDelay = -1;
    double iti = -1;
    double userReaction = -1; //sec
    double preImaq = -1;

    /*Stimulus properties*/
    double soundDurationPositive = -1;
    double soundDurationNegative = -1;
    QString fileNamePositive;
    QString fileNameNegative;

    /*Reward*/
    int licksToReward;
};





#endif // SETTINGSTRUCTURES_H
