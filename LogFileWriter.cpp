#include "LogFileWriter.h"

LogFileWriter::LogFileWriter(QObject* parent)
    : QObject(parent)
{
}

void LogFileWriter::onOpen(QString fileName)
{
    logFile.setFileName(fileName);
    logFile.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&logFile);
    /*Write header*/
    out<<QString("#event\tbit_mask\tstate\ttime\n");
}

void LogFileWriter::onClose()
{
    logFile.close();
}

void LogFileWriter::onWrite(QString text, uint id, uint counter, QString timeString)
{
//    QString timeString = QTime::currentTime().toString("hh:mm:ss.zzz");
    if(logFile.isOpen()){
        QTextStream out(&logFile);
        out << text << "\t" << id << "\t" << counter << "\t" << timeString << "\n";
    }
}

void LogFileWriter::onWriteTrackingResult(QVector<double> trackingResult)
{
    if(logFile.isOpen()){
        QTextStream out(&logFile);
        out << "Tracking";
        for(double r: trackingResult){
            out << "\t" << r;
        }
        out << "\n";
    }
}
