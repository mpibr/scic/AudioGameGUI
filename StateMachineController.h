#ifndef STATEMACHINECONTROLLER_H
#define STATEMACHINECONTROLLER_H
#include <QObject>
#include <QTimer>
#include <QElapsedTimer>
#include <QVector>
#include <QStateMachine>
#include <QState>
#include <QHistoryState>
#include <QDebug>
#include <QTime>
#include <QMessageBox>

#include <ctime>
#include <algorithm> //std::random_shuffle

#include "SettingStructures.h"
#include "TimerDialog.h"
#include "NIDAQmxInterface.h"
#include "TDTInterface.h"
#include "SocketClient.h"
#include "rpcoxlib.h"
#include "LogFileWriter.h"

class StateMachineController : public QObject
{
    Q_OBJECT

public:
    explicit StateMachineController(QObject *parent = 0);
    ~StateMachineController();
    audioGameSettings settings;
    void startStateMachine(audioGameSettings);
    void pauseStateMachine();
    void stopStateMachine();

private:
    QStateMachine *stateMachine; /* state machine */
    QState *state_preDelay;
    QState *state_iti;
    QState *state_userReaction;
    QState *state_acquisitionBaseline;
    QState *state_sound;

    QTimer *mainTimer; /*the timer that triggers the state changes*/
    TimerDialog *timerDialog;
    NIDAQmxInterface* nidaq;
    TDTInterface* tdt;
    SocketClient* socketClient;
    QThread* socketThread;
    QThread* logFileThread;
    LogFileWriter* logFileWriter;

    int trialsTotal = -1;
    int trialsCurrent = -1; /* is gonna be 1-based, so first trial has index 1 */

private slots:
    void onStatePreDelayEntered();
    void onStateItiEntered();
    void onStateUserReactionEntered();
    void onStateUserReactionExited();
    void onStateAcquisitionBaselineEntered();
    void onStateSoundEntered();
    void onStatePauseEntered();
    void onStatePauseExited();


signals:
    void statusMessage(QString);
    void terminated();
    void pauseRequested();

    void openLogFile(QString fileName);
    void closeLogFile();
    void writeToLogFile(QString text, uint id, uint counter, QString stringTime);
};

#endif // STATEMACHINECONTROLLER_H
