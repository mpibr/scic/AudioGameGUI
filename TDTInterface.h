#ifndef TDTInterface_h
#define TDTInterface_h

#include <stdio.h>
#include <stdlib.h>
#include "rpcoxlib.h"

class TDTInterface
{
public:
    static const int TDT_STATUS_NULL = 0x00;
    static const int TDT_STATUS_CONNECTED = 0x01;
    static const int TDT_STATUS_CIRCUIT_LOADED = 0x02;
    static const int TDT_STATUS_CIRCUIT_RUNNING = 0x04;

    /* funciton prototypes */
    int initializeInterface();
    void loadRCXCircuit(QString fileName);
    void destroyInterface();

    RPCOXLib::RPcoX rp;
    HRESULT hr;
    long int tdt_status;

};

#endif /* define (TDTInterface_h) */
