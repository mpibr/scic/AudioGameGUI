#ifndef LOGFILEWRITER_H
#define LOGFILEWRITER_H

#include <QObject>
#include <QFile>
#include <QThread>
#include <QDataStream>
#include <QDebug>
#include <QTime>
#include <QVector>

class LogFileWriter : public QObject
{
    Q_OBJECT
public:
    LogFileWriter(QObject *parent = 0);
private:
    QFile logFile;
    QIODevice* device;
    QTime qTime;
private slots:
    void onOpen(QString fileName);
    void onClose();
    void onWrite(QString text, uint id, uint counter, QString timeString);
    void onWriteTrackingResult(QVector <double>);
};

#endif // LOGFILEWRITER_H
