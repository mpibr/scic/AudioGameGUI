#ifndef TIMERDIALOG_H
#define TIMERDIALOG_H

#include <QDialog>
#include <QTimer>

namespace Ui {
class TimerDialog;
}

class TimerDialog : public QDialog
{
    Q_OBJECT

    public:
        explicit TimerDialog(QWidget *parent = 0);
        ~TimerDialog();
        int timeValue;
        void setUpTimer(int);
        void updateTime();
        void startTimer();
        void stopTimer();

        QTimer *timerDialogTimer;

    public slots:
        void removeTimer();

    private slots:
        void onCountDown();

    private:
        Ui::TimerDialog *ui;
};

#endif // TIMERDIALOG_H
