#include "AboutDialog.h"
#include "ui_AboutDialog.h"
#include <QDebug>
AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);
    ui->label_title->setText("AudioGame " + QString(APP_VERSION));
    ui->label_mpiLogo->setPixmap(QPixmap(":/logos/mpi-brain-research.png"));
    ui->label_scicLogo->setPixmap(QPixmap(":/logos/logo_scic.png"));
    ui->label_audioGameLogo->setPixmap(QPixmap(":/logos/AudioGame.png"));
}

AboutDialog::~AboutDialog()
{
    delete ui;
}
