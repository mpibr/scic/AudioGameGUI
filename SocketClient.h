#ifndef SOCKETCLIENT_H
#define SOCKETCLIENT_H
#include <QDialog>
#include <QTcpSocket>
#include <QDataStream>
#include <QtWidgets>
#include <QtNetwork>
#include <QTimer>

class QComboBox;
class QLabel;
class QLineEdit;
class QPushButton;
class QTcpSocket;
class QNetworkSession;

/*Parameters used as Type in outgoing packets*/
#define SOCKET_TIMESTAMP 0
#define SOCKET_START 1
#define SOCKET_STOP 2
#define SOCKET_FILENAME 3
#define SOCKET_TRACKINGRESULT 4

class SocketClient : public QObject
{
    Q_OBJECT

public:
    explicit SocketClient(QObject *parent = Q_NULLPTR);
    void connectToHost();

private slots:
    void onReadData();
    void onConnected();
    void onDisconnected();

    void onSocketError();

private:
    QTcpSocket *tcpSocket;
    QString hostname;
    int port;
    void readMax(QIODevice *io, int n);
    QBuffer outBuffer;
    QBuffer inBuffer;
    QDataStream outStream; /*To serialize outgoing data*/
    QDataStream inStream; /*To serialize incoming data*/

signals:
    void socketTimeStamp(quint64 timeStamp);
    void socketStartTimeStamp(quint64 timeStamp);
    void socketStopTimeStamp(quint64 timeStamp);
    void socketFileName(QString fileName);
    void socketTrackingResult(QVector<double> trackingResult);

};

#endif // SOCKETCLIENT_H
